# AI4CE - A platform to validate AI-based System Generation

This repo consists of the main GUI for the AI4CE project.

# Use the CLI
``` bash
poetry install
poetry run python ai4ce_cli.py update
poetry run python ai4ce_cli.py up  # then acknowledge with enter

# To rebuild all or one specific component
poetry run python ai4ce_cli.py rebuild # then acknowledge with enter and select desired option

# To shut down the whole setup
poetry run python ai4ce_cli.py down

# To get help
poetry run python ai4ce_cli.py --help
```




# How to Update
``` bash
docker image build -t ai4ce:01 .
docker compose up
```
