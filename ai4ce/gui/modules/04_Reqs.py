from pathlib import Path

import streamlit as st

iframe_code = f'<iframe src="http://localhost:85{Path(__file__).name[:2]}" width=1500 height=3000></iframe>'
st.markdown(iframe_code, unsafe_allow_html=True)
