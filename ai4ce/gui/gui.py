"""
Demo class for the streamlit GUI.
"""
import json
from pathlib import Path
import httpx
import streamlit as st
import tomllib
import plotly.express as px
import plotly.graph_objects as go

import pandas as pd
import numpy as np

from ai4ce.helpers import enable_component, get_enabled_components, get_list_of_all_project_infos, get_prepared_system_generator_info, get_recent_project, get_sys_arch, load_file, set_new_project, set_sys_arch, translate_tomlsystem_to_backend, traverse_and_modify
from ai4ce.main import calculate_orbit_velocity
from dataclasses import dataclass

import hydralit_components as hc

from ai4ce.gui.elements.welcome import welcome
from ai4ce.gui.elements.load_project import load_project
from ai4ce.gui.elements.project_summary import project_summary
from ai4ce.gui.elements.module_description_table import module_table
# from ai4ce.gui.elements.sidebar import sidebar


# st.set_page_config(layout="wide", page_title="AI4CE System Generator")

# sidebar()

base_url: str = "http://localhost"
# base_url: str = "http://backend"


@dataclass
class App:
    name: str
    title: str
    description: str


def app():
    if 'project_name' not in st.session_state:
        st.session_state['project_name'] = "None Selected"
        st.session_state['project_id'] = None

    app_dict = {
        0: App(
            name="AI4CE",
            title="AI4CE",
            description="Welcome to the AI4CE System Generator!"
        ),
        99: App(
            name="Create Project",
            title="Create Project",
            description="Create a new CubeSat project."
        ),
        # 1: App(
        #     name="System Architecture",
        #     title="CubSat System Architecture: Definition of Evaluation Functions",
        #     description="The CubSat System Architecture project, where Johannes developed the underlying evaluation functions for each CubeSat compoents."
        # ),
        2: App(
            name="g3Del",
            title="g3Del - Generative 3D model: See a conceptual 3D model of your satellite",
            description="The g3Del team lead by Moritz developed a tool that can generate 3D models for a given component list."
        ),
        # 3: App(
        #     name="Backend",
        #     title="Backend",
        #     description="This is the Backend App."
        # ),
        # 4: App(
        #     name="Requirements",
        #     title="Requirements",
        #     description="This is the Requirements App."
        # ),
        5: App(
            name="Build System Generator",
            title="Reinforcement Learning",
            description="With the Reinforcement Learning module developed by Younes"
                        "the user can create the System Generators."
                        "\n\nThis means that the AI agents can be created with all"
                        "their hyperparametrs and before the training process can"
                        "be started.XXXXX",
        ),
        6: App(
            name="AI Benchmarking",
            title="AI Benchmarking",
            description="This is the AI Benchmarking App."
        ),
        # 7: App(
        #     name="CS Benchmarking",
        #     title="CS Benchmarking",
        #     description="This is the CS Benchmarking App."
        # ),
        # 8: App(
        #     name="System Diagram",
        #     title="System Diagram",
        #     description="This is the System Diagram App."
        # ),
        9: App(
            name="Flight Dynamics",
            title="Flight Dynamics",
            description="This is the Flight Dynamics App."
        ),
        10: App(
            name="BP compdb2",
            title="BP compdb2",
            description="This is the BP compdb2 App."
        ),
        11: App(
            name="MBSE Export",
            title="MBSE Export",
            description="This is the MBSE Export App."
        ),
        12: App(
            name="geseto",
            title="geseto",
            description="This is the GESETO App."
        ),
        13: App(
            name="Ops 2 Design",
            title="Ops 2 Design",
            description="This is the Ops 2 Design App."
        ),
        16: App(
            name="DTreqs",
            title="Digital Twin Requirements",
            description="Making the first steps towards a digital twin world"
        ),
        98: App(
            name="Project Summary",
            title="Project Summary",
            description="A summary of the whole project."
        ),
        66: App(
            name="AI4CE Playground",
            title="AI4CE Playground",
            description="A playground to test new concepts"
        ),
        77: App(
            name="Repo Template",
            title="Repo Template",
            description="The default template for all repos"
        ),
    }

    menu_data = [{'id': app[0], 'label': app[1].name}
                 for app in list(app_dict.items())]
    menu_data[0].update({'icon': "far fa-copy"})

    over_theme = {'txc_inactive': 'white',
                  'txc_active': 'black', 'menu_background': '#055471'}
    menu_id = hc.nav_bar(
        menu_definition=menu_data,
        override_theme=over_theme,
        hide_streamlit_markers=True,  # hide when deployed
        sticky_nav=True,  # at the top or not
        sticky_mode='pinned',  # jumpy or not-jumpy, but sticky or pinned
    )

    match menu_id:
        case 0 | 'AI4CE':  # for some reason the menu_id starts as 0, but subsequently becomes 'AI4CE'
            welcome()

            plantuml_url = "https://cdn-0.plantuml.com/plantuml/png/VLHHRzis47xdhpWWmE06o9PsNH_aB6WYDKMn10lYRNanC33IMU8gaXf9nNFd_VVnHB5q5ALvOlHzTv_lxhjxRnqthfSj6nuCm6w5AnjOv0Qvm_srvXNS6lqNbkwLXm5ARX4MPrPymUprs_FZuy0yFZxxWRwT1nP0RNJVmMfd7Kgu5r-ugTW80Hnj6n-KXT2O7uI2BZoBosNYNekwSHyDeifMs1AHM-2b4uzVGnCxAt0Z54RX2rCsmdb6Rt0vaO_K6s6jq0f-DG_YCDDGk-VmldLmXrjV0LprgdIUV43xK8blmIkd3KxnsL9HLQ5hSntyY0eDzxb6C4vg0R7XSJYBEb1nhRX_kaCSO6nXocUNJ_5SguseUuClKtupN2YueEGHIkgjxzfQILrXEr6kqS7m7JP6oxKQ6m2n0sCFGbq7_Vq7ozuXt51eOXhiaBkd2biZFB7Kick5TU0q98rf96mRqI8ihhG1v6K3TcWCLc1t5iOUqgWakaPNhq1OICrEivhT2RBEP2Fczk55Wy-x-wZZBTO3ZmHUz7XoED7c_Psj6hq5EK8wGQcoRtemWNVNWEfH6AqaAWUwewxPaRBHvZERU6-jYZnV7X20rroeI3iadhx-C_VYLE7B0OcUzzPfUR1FWVslDl_DpYsj-MG_xUpv2TJ1_LW5FyxpheINkKJhG461LjJAmbe1BqjjAg5gScOTc_diBAtu8oOtLcEyKDx4Fia9tFIj4rsBAOM53atqEd56yHXCS6B3yuEJrevbuvGHVYn0XXfP66CWmyKdABXj7dIoC41seNYxiy9wcEHyXEIgHD9AEfGfZI-v2sBqa4ziO97t3xZYxXMu1jKJl-Mhn7wcJcWeUlwgOclnIROQBJezayZ_gaacW30QJXGdIXh99Iy_UuLvCVfFRDYaqM4ds5XxBoKtEqO-5DR5JRl3ltjXK8uNDe1XWK6eZHxqZkw3NMYHDK7Jhyb4ipuWUk1z1xcM7JS8l-1kT2mKHZnwEY-DjZP6XVFeKDeKdNmIjobso0wNdaO83wbHoM5qhnqgkiZQE8_HjQSdBSXjSRLa1zPfiQR_3AqP-xshwCTayERqpTkJqtSdfp-nFz3GCSlWzFjtZEr_pdMhpPxAAOqOhXBisVxiA1_-pi7lwWxsq7UbbhIK18OLFFUjvgf6Zzv_kWr0MkyX4jKUpafIoCB7GYlqJ78rvSs-vcK0EcJBA5kC2a9t7me68P-7AGUL74fbZ5cdE_RUltlP_Wy0"
            # iframe_code = f'<iframe src="{plantuml_url}" width="700" height="400"></iframe>'
            iframe_code = f'<iframe src="{plantuml_url}" width=1000 height=3000></iframe>'
            st.markdown(iframe_code, unsafe_allow_html=True)

            module_table()

        case 98:
            project_summary()
            pass

        case 99:
            load_project()

            st.divider()
            plantuml_url = "https://www.plantuml.com/plantuml/png/bLInRjim4Dtv5G_8OWyfqY3e82znx2O8aA11tJQB5qOwIsmfKY1FSLFquqjAfbKRCefkfDwxTxntpxxsB1stjHRTGG1-gpYlC5auaao_7wmiyEJiTyfv6c0WbvuIt7y1IcVR1gitprJZMVsIhX0xw6nR1MRMyvCIb47J7vStiyHyK6N5Tux8P2lIaIOzPCxgTL-OkDcIDifGQZbtUQKuuAsZsK7ZH7nKtYjhyCczg46bcoumPAiPdsahNO7RrkGSg7lInqAT1bTi7HsZe-EHANgVnZox8qDE1gqTcFOmGR8uMJyfUogjZGn7_Xi7ad5xFJyxn1VMR5GPR3fD-UAaCfX7yGGDtNLmR6rgMv0-wbmIe_kEZRFrskmCG7GWCQC9_M02IFR-fBpbiA5OEnEE6f9yABrrAX1oMpTQUGPR3EAf0jjAQSBarZgGp2lupWaguDyyUjEwtTJ4bIscK1xHtP7jf8p7T4p5MBPF1tOqhMUDr5HslAZvxzHsjEFC_b-0qkk6hjrUgKX6Z_5kLUXnGvvXi2IlIkEnDf1vRbsXJ1cjBrEdlKHwz4g-qc1xiQjN9cmfY9pZiTMi6as3X4T3BYqpSbBpL1pWWOsndzAGg8rrFqIoPNwFfVJLYntM1MHp8_MRLpx2GJfKrDBadVc3PYS4MGQt10QNaOexERN1PD6-q4ho55oHELZAOhLdZpGFQ3_V-5y3QKy7QYLvOZiY55vrKYdTrEPWk7ZgZ192KnblBgvVV6qAoLHakBoul3g_k3f__q5y8nVpb-7YtQK0h9Ec96HuldyArounuRQqXg9iBnTqFTj6N8TpM-i_"
            con1 = st.container(height=1700)
            iframe_code = f'<iframe src="{plantuml_url}" width="1200" height="1700"></iframe>'
            con1.markdown(iframe_code, unsafe_allow_html=True)

            pass
        case x if x in list(app_dict.keys())[1:]:
            st.title(app_dict[x].title)
            st.write(app_dict[x].description +
                     f"\n\nYou can see the project in Fullscreen here: {base_url}:{str(8500 + x)}")
            st.markdown(
                f'<iframe src="{base_url}:{str(8500 + x)}" width="100%" height="1700px"></iframe>', unsafe_allow_html=True)
        case _:
            st.markdown('Something went wrong...')


# # This check allows the script to be run standalone (useful for development/testing)
# if __name__ == "__main__":
#     app()
