import httpx
from pathlib import Path

import streamlit as st

from ai4ce.helpers import get_enabled_components, get_list_of_all_project_infos, get_recent_project, get_sys_arch, load_file, set_new_project, translate_tomlsystem_to_backend


def load_project():

    # first row with the project selector
    if 'project_name' not in st.session_state:
        st.session_state['project_name'] = "None Selected"
        st.session_state['project_id'] = None
        st.session_state["modified"] = None

    proj_list = get_list_of_all_project_infos()
    project_idname_list = [("ID: ", project["id"], project["project_name"], project["modified"][:16])
                           for project in proj_list]

    col01, col02, col03, col04 = st.columns([5, 1, 2, 2])
    with col01:
        st.title("Load or Create a new CubeSat project")
        # st.title(
        #     f"Summary of the System Generation for {st.session_state['project_name']}")

    with col02:
        if st.button("Get recent project"):
            (st.session_state["project_id"],
             st.session_state["project_name"],
             st.session_state["modified"]) = get_recent_project()
    with col03:
        # Let the user select a project from the list of projects in the project DB
        selected_project = st.selectbox("Select a project",
                                        project_idname_list,
                                        index=None,
                                        placeholder=st.session_state["project_name"])
        if selected_project is not None:
            st.session_state["project_id"] = selected_project[1]
            st.session_state["project_name"] = selected_project[2]
    with col04:
        st.metric(label=f"Active project ID: {st.session_state['project_id']}",
                  value=f"ID: {st.session_state['project_id']} - {st.session_state['project_name']}")

    st.title("Load a Use Case")
    st.markdown("""
                To showcase the functionality of the AI4CE system generator hub, selected Use Cases can be loaded. This includes
                - Project, Mission/Orbit and System Description
                - pre-trained AI models
                - pre-run brute force algorithm
                """)

    usecase_col1, usecase_col2 = st.columns(2)

    # =============
    # OPS-Sat 3comp
    # =============
    with usecase_col1:
        usecase_col1_left, usecase_col1_right = st.columns(2)

        with usecase_col1_left:
            st.image(
                "https://www.thalesgroup.com/sites/default/files/database/assets/images/2021-12/OPS-SAT_ESA_s_flying_lab_open_to_all_pillars_1100.jpg")

        with usecase_col1_right:
            st.subheader("ESA's OPS-Sat CubeSat - 3comp")
            st.write(
                "OPS-Sat is a 3U CubeSat mission by ESA/ESOC to demonstrate new ways to handle on-board experiments.")

            # st.markdown(
            #     ''' **battery, solar-panel, reaction-wheel**''')

            st.toggle("Battery", value=True, key="3comp_bat_")
            st.toggle("Camera", value=False, key="3comp_cam_")
            st.toggle("Reaction Wheel", value=True, key="3comp_rw_")
            st.toggle("Solar Panel", value=True, key="3comp_sp_")
            st.toggle("Transceiver", value=False, key="3comp_tran_")

            if st.button("Load OPS-Sat 3comp", key="3comp_button_"):

                project_config = load_file(filename=Path(
                    "ai4ce/project_config_opssat.toml"))
                # st.write(project_config["Project"]["Project_Name"])

                project_info = {}
                project_info["project_name"] = project_config["Project"]["Project_Name"]
                project_info["description"] = project_config["Project"]["Description"]
                project_info["website"] = project_config["Project"]["Website"]
                project_info["picture_web"] = project_config["Project"]["Picture_web"]
                project_info["project_opensource"] = False

                enabled_components = get_enabled_components(
                    project_config)
                st.expander("Enabled Components").write(
                    enabled_components)

                msg = set_new_project(project_info=project_info)
                # st.write(msg)
                st.session_state['project_id'] = msg["id"]
                st.session_state['project_name'] = msg["project_name"]

                resp = translate_tomlsystem_to_backend(
                    system=project_config["System"], project_id=st.session_state["project_id"])
                st.expander("Response", expanded=False).write(resp)

                with st.expander("Get Sys Arch from backend"):
                    st.write(get_sys_arch(
                        project_id=st.session_state["project_id"]))

                st.success(
                    f"Set {st.session_state['project_name']} as active project @ project ID {st.session_state['project_id']}")

        # =============
        # OPS-Sat 5comp
        # =============
    with usecase_col2:
        usecase_col2_left, usecase_col2_right = st.columns(2)

        with usecase_col2_left:
            st.image(
                "https://www.thalesgroup.com/sites/default/files/database/assets/images/2021-12/OPS-SAT_ESA_s_flying_lab_open_to_all_pillars_1100.jpg")

        with usecase_col2_right:
            st.subheader("ESA's OPS-Sat CubeSat - 5comp")
            st.write(
                "OPS-Sat is a 3U CubeSat mission by ESA/ESOC to demonstrate new ways to handle on-board experiments.")
            # st.markdown(
            #     ''' **battery, solar-panel, reaction-wheel, transceiver, camera**''')

            st.toggle("Battery", value=True)
            st.toggle("Camera", value=True)
            st.toggle("Reaction Wheel", value=True)
            st.toggle("Solar Panel", value=True)
            st.toggle("Transceiver", value=True)

            if st.button("Load OPS-Sat 5comp"):
                project_config = load_file(filename=Path(
                    "ai4ce/project_config_opssat.toml"))
                # st.write(project_config["Project"]["Project_Name"])

                project_info = {}
                project_info["project_name"] = project_config["Project"]["Project_Name"]
                project_info["description"] = project_config["Project"]["Description"]
                project_info["website"] = project_config["Project"]["Website"]
                project_info["picture_web"] = project_config["Project"]["Picture_web"]
                project_info["project_opensource"] = False

                enabled_components = get_enabled_components(
                    project_config)
                st.expander("Enabled Components").write(
                    enabled_components)

                msg = set_new_project(project_info=project_info)
                # st.write(msg)
                st.session_state['project_id'] = msg["id"]
                st.session_state['project_name'] = msg["project_name"]

                resp = translate_tomlsystem_to_backend(
                    system=project_config["System"], project_id=st.session_state["project_id"])
                st.expander("Response", expanded=False).write(resp)

                with st.expander("Get Sys Arch from backend"):
                    st.write(get_sys_arch(
                        project_id=st.session_state["project_id"]))

                st.success(
                    f"Set {st.session_state['project_name']} as active project @ project ID {st.session_state['project_id']}")

    col11, col12 = st.columns(2)
    sys_config_enabled = {}

    # with col11:

    # col111, col112 = st.columns(2)
    # with col111:
    #     st.image(
    #         "https://www.thalesgroup.com/sites/default/files/database/assets/images/2021-12/OPS-SAT_ESA_s_flying_lab_open_to_all_pillars_1100.jpg")
    # with col112:
    #     st.subheader("ESA's OPS-Sat CubeSat - 3comp")
    #     st.write(
    #         "OPS-Sat is a 3U CubeSat mission by ESA/ESOC to demonstrate new ways to handle on-board experiments.")

    #     # st.markdown(
    #     #     ''' **battery, solar-panel, reaction-wheel**''')

    #     st.toggle("Battery", value=True, key="3comp_bat")
    #     st.toggle("Camera", value=False, key="3comp_cam")
    #     st.toggle("Reaction Wheel", value=True, key="3comp_rw")
    #     st.toggle("Solar Panel", value=True, key="3comp_sp")
    #     st.toggle("Transceiver", value=False, key="3comp_tran")

    #     if st.button("Load OPS-Sat 3comp"):

    #         project_config = load_file(filename=Path(
    #             "ai4ce/project_config_opssat.toml"))
    #         # st.write(project_config["Project"]["Project_Name"])

    #         project_info = {}
    #         project_info["project_name"] = project_config["Project"]["Project_Name"]
    #         project_info["description"] = project_config["Project"]["Description"]
    #         project_info["website"] = project_config["Project"]["Website"]
    #         project_info["picture_web"] = project_config["Project"]["Picture_web"]
    #         project_info["project_opensource"] = False

    #         enabled_components = get_enabled_components(
    #             project_config)
    #         st.expander("Enabled Components").write(
    #             enabled_components)

    #         msg = set_new_project(project_info=project_info)
    #         # st.write(msg)
    #         st.session_state['project_id'] = msg["id"]
    #         st.session_state['project_name'] = msg["project_name"]

    #         resp = translate_tomlsystem_to_backend(
    #             system=project_config["System"], project_id=st.session_state["project_id"])
    #         st.expander("Response", expanded=False).write(resp)

    #         with st.expander("Get Sys Arch from backend"):
    #             st.write(get_sys_arch(
    #                 project_id=st.session_state["project_id"]))

    #         st.success(
    #             f"Set {st.session_state['project_name']} as active project @ project ID {st.session_state['project_id']}")

    # col121, col122 = st.columns(2)
    # with col121:
    #     st.image(
    #         "https://www.thalesgroup.com/sites/default/files/database/assets/images/2021-12/OPS-SAT_ESA_s_flying_lab_open_to_all_pillars_1100.jpg")
    # with col122:
    #     st.subheader("ESA's OPS-Sat CubeSat - 5comp")
    #     st.write(
    #         "OPS-Sat is a 3U CubeSat mission by ESA/ESOC to demonstrate new ways to handle on-board experiments.")
    #     # st.markdown(
    #     #     ''' **battery, solar-panel, reaction-wheel, transceiver, camera**''')

    #     st.toggle("Battery", value=True)
    #     st.toggle("Camera", value=True)
    #     st.toggle("Reaction Wheel", value=True)
    #     st.toggle("Solar Panel", value=True)
    #     st.toggle("Transceiver", value=True)

    #     if st.button("Load OPS-Sat 5comp"):
    #         project_config = load_file(filename=Path(
    #             "ai4ce/project_config_opssat.toml"))
    #         # st.write(project_config["Project"]["Project_Name"])

    #         project_info = {}
    #         project_info["project_name"] = project_config["Project"]["Project_Name"]
    #         project_info["description"] = project_config["Project"]["Description"]
    #         project_info["website"] = project_config["Project"]["Website"]
    #         project_info["picture_web"] = project_config["Project"]["Picture_web"]
    #         project_info["project_opensource"] = False

    #         enabled_components = get_enabled_components(
    #             project_config)
    #         st.expander("Enabled Components").write(
    #             enabled_components)

    #         msg = set_new_project(project_info=project_info)
    #         # st.write(msg)
    #         st.session_state['project_id'] = msg["id"]
    #         st.session_state['project_name'] = msg["project_name"]

    #         resp = translate_tomlsystem_to_backend(
    #             system=project_config["System"], project_id=st.session_state["project_id"])
    #         st.expander("Response", expanded=False).write(resp)

    #         with st.expander("Get Sys Arch from backend"):
    #             st.write(get_sys_arch(
    #                 project_id=st.session_state["project_id"]))

    #         st.success(
    #             f"Set {st.session_state['project_name']} as active project @ project ID {st.session_state['project_id']}")

    st.divider()

    project_info = {}

    # with col12:
    st.title("Create a new CubeSat project")
    project_info["project_name"] = st.text_input("Project Name")
    project_info["project_description"] = st.text_area(
        "Project Description")
    project_info["project_website"] = st.text_input(
        "Project Website", placeholder="https://ai4ce.space")
    project_info["project_picture"] = st.text_input(
        "Project Picture", placeholder="https://ai4ce.space/cube_sat_rendering.png")
    project_info["project_opensource"] = st.radio(
        label="Is your project open source?", options=["Yes.", "Not yet."], horizontal=True)

    if st.button("Create a new Project.", key="custom_button", args={"class": "custom-button"}):
        msg = set_new_project(project_info=project_info)
        st.success(msg)
    st.divider()

    clicked = st.button("Create 7 Example projects.", key="button", args={
                        "class": "custom-button"})

    if clicked:
        # URL and query parameters
        url = "http://backend:8000/api/v1/testing/create_project"
        params = {
            "number_of_projects": 7,
            "max_components": 10,
            "create_components": "true"
        }

        # Headers
        headers = {
            "accept": "application/json"
        }

        # Make the POST request
        response = httpx.post(
            url, params=params, headers=headers, content='')

        st.write(response.json())
