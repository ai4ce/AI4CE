from collections import defaultdict
from datetime import datetime
from pathlib import Path
import toml

import matplotlib.pyplot as plt
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import streamlit as st
from streamlit_extras.metric_cards import style_metric_cards


MODULE_DESCRIPTION_PATH: Path = Path("ai4ce/gui/modules.toml")


def dashboard() -> None:
    """Function to display the dashboard.

    This function serves as the main entry point for the dashboard application. It is responsible for loading data,
    calculating various metrics, and visualizing the data using different plots and charts.

    Parameters:
        None

    Returns:
        None
    """
    # def load_data(filepath: Path) -> pd.DataFrame:
    #     """Load project data from a TOML file into a DataFrame.
    #     Params:
    #         filepath (Path): The path to the TOML file.

    #     Returns:
    #         pd.DataFrame: DataFrame with parsed project data and datetime type for dates.
    #     """
    #     with open(filepath, 'r') as file:
    #         data = toml.load(file)
    #     modules = []
    #     for key, value in data['Modules'].items():
    #         if isinstance(value, dict):
    #             value['module_key'] = key
    #             modules.append(value)
    #     df = pd.DataFrame(modules)
    #     df['date_start'] = pd.to_datetime(df['date_start'])
    #     df['date_end'] = pd.to_datetime(df['date_end'])
    #     return df

    def load_data_from_file(filepath: Path) -> dict:
        with open(filepath, 'r') as file:
            data = toml.load(file)
        return data

    def load_data(filepath: Path) -> pd.DataFrame:
        data = load_data_from_file(filepath=filepath)

        modules = []
        for key, value in data['Modules'].items():
            if isinstance(value, dict):
                value['module_key'] = key
                # Ensuring 'type' exists and categorizing accordingly
                if 'type' in value:
                    if value['type'] in ['BT', 'BP']:
                        value['level'] = 'Bachelor'
                    elif value['type'] in ['ADP', 'MT']:
                        value['level'] = 'Master'
                    else:
                        # Handle other types or unknowns
                        value['level'] = 'Other'
                else:
                    # Default if type is not specified
                    value['level'] = 'Unknown'
                modules.append(value)
        df = pd.DataFrame(modules)
        df['date_start'] = pd.to_datetime(df['date_start'])
        df['date_end'] = pd.to_datetime(df['date_end'])
        return df

    # define a function of the sum of modules and students with the start and end date per day and return the values as a time series
    # add docstrings and type hints to the function

    def count_modules_and_students_timeseries(start_date, end_date, modules: pd.DataFrame):
        date_range = pd.date_range(start=start_date, end=end_date)

        counts = []
        for date in date_range:
            active_modules = ((modules['date_start'] <= date) & (
                modules['date_end'] >= date)).sum()
            active_students = modules.loc[(modules['date_start'] <= date) & (
                modules['date_end'] >= date), 'team_size'].sum()
            counts.append({'Date': date, 'Active Modules': active_modules,
                           'Active Students': active_students})
        return pd.DataFrame(counts)

        # """Count the total number of modules and students within a date range.
        # Params:
        #     start_date (pd.Timestamp): The start date of the date range.
        #     end_date (pd.Timestamp): The end date of the date range.
        #     modules (pd.DataFrame): The DataFrame containing module data with 'date_start' and 'date_end'.
        # Returns:
        #     pd.DataFrame: DataFrame with the total number of modules and students per day.
        # """
        # date_range = pd.date_range(start=start_date, end=end_date)
        # counts = []
        # for date in date_range:
        #     active_modules = ((modules['date_start'] <= date) & (modules['date_end'] >= date)).sum()
        #     active_students = modules.loc[(modules['date_start'] <= date) & (modules['date_end'] >= date), 'team_size'].sum()
        #     counts.append({'Date': date, 'Active Modules': active_modules, 'Active Students': active_students})
        # return pd.DataFrame(counts)

    # Function to count active modules and sum of team sizes

    def count_modules_and_students(start_date, end_date, df):
        active_modules_df: int = df[(df['date_start'] <= end_date) & (
            df['date_end'] >= start_date)]

        today = pd.Timestamp(datetime.now().date())
        active_today_df = df[(df['date_start'] <= today)
                             & (df['date_end'] >= today)]

        total_modules: int = df.shape[0]
        total_students: int = df['team_size'].sum()

        active_modules: int = active_modules_df.shape[0]
        active_students: int = active_modules_df['team_size'].sum()

        today_modules: int = active_today_df.shape[0]
        today_students: int = active_today_df['team_size'].sum()

        return total_modules, total_students, active_modules, active_students, today_modules, today_students

    # plot the modules and students timeseries using plotly, putting the modules and students on diffrent scales

    # import plotly.graph_objects as go

    # # Create a trace for modules
    # trace1 = go.Scatter(
    #     x=modules_students_timeseries.index,
    #     y=modules_students_timeseries['Active Modules'],
    #     name='Modules',
    #     line=dict(color='blue')
    # )

    # # Create a trace for students
    # trace2 = go.Scatter(
    #     x=modules_students_timeseries.index,
    #     y=modules_students_timeseries['Active Students'],
    #     name='Students',
    #     line=dict(color='red'),
    #     yaxis='y2'
    # )

    # data = [trace1, trace2]

    # layout = go.Layout(
    #     title='Modules and Students Timeseries',
    #     yaxis=dict(
    #         title='Number of Modules'
    #     ),
    #     yaxis2=dict(
    #         title='Number of Students',
    #         titlefont=dict(color='red'),
    #         tickfont=dict(color='red'),
    #         overlaying='y',
    #         side='right'
    #     )
    # )

    # fig = go.Figure(data=data, layout=layout)

    # # Display the figure
    # st.plotly_chart(fig)

    # ====================================================
    # ================ Stacked Bars ======================
    # ====================================================

    def plot_module_activity(df, start_date, end_date):
        """Plot the activity of modules over time using a bar chart within specified dates.
        Params:
            df (pd.DataFrame): DataFrame containing module data with 'date_start' and 'date_end'.
            start_date (pd.Timestamp): User-selected start date.
            end_date (pd.Timestamp): User-selected end date.
        """
        # Filter the DataFrame for modules that have any activity within the given date range
        filtered_df = df[(df['date_end'] >= start_date) &
                         (df['date_start'] <= end_date)]

        # Adjust start and end dates for display
        filtered_df['display_start'] = filtered_df['date_start'].apply(
            lambda x: max(x, start_date))
        filtered_df['display_end'] = filtered_df['date_end'].apply(
            lambda x: min(x, end_date))

        # Sorting the DataFrame by 'module_key' in alphabetical order
        filtered_df = filtered_df.sort_values(by='module_key')

        # Create a Gantt-like plot using Plotly Express
        fig = px.timeline(filtered_df, x_start="display_start", x_end="display_end", y="module_key", color="module_key",
                          labels={'module_key': 'Module Key'}, title="Module Activity Within Selected Dates")
        fig.update_layout(
            autosize=False,
            width=1100,
        )

        # Reverse the order of modules from A to Z (top to bottom)
        fig.update_layout(
            xaxis_title='Date',
            yaxis_title='Module',
            yaxis={'categoryorder': 'array', 'categoryarray': filtered_df['module_key'].tolist()[
                ::-1]},
            height=600
        )

        return fig

    # ======================================================================================

    def count_active_modules_per_day(df, start_date, end_date):
        """Generate daily active module counts within a date range."""
        date_range = pd.date_range(start=start_date, end=end_date)
        active_counts = []
        for single_date in date_range:
            count = ((df['date_start'] <= single_date) &
                     (df['date_end'] >= single_date)).sum()
            active_counts.append(count)
        return pd.DataFrame({'Date': date_range, 'Active Modules': active_counts})

    # TreeMap

    def load_and_count_levels_and_subjects(filepath: Path) -> dict:
        with open(filepath, 'r') as file:
            data = toml.load(file)
        level_counts = defaultdict(
            lambda: {'total_students': 0, 'subjects': defaultdict(int)})
        level_mapping = {
            'BT': 'Bachelor',
            'BP': 'Bachelor',
            'ADP': 'Master',
            'MT': 'Master'
        }
        for module in data['Modules'].values():
            if isinstance(module, dict) and 'type' in module:
                level_group = level_mapping.get(module['type'], 'Other')
                level_counts[level_group]['total_students'] += module.get(
                    'team_size', 0)
                if 'stud_subject' in module:
                    for subject in module['stud_subject']:
                        level_counts[level_group]['subjects'][subject] += module.get(
                            'team_size', 0)
        return {k: dict(v, subjects=dict(v['subjects'])) for k, v in level_counts.items()}

    def prepare_data_for_treemap(level_and_subject_counts):
        rows = []
        for level, data in level_and_subject_counts.items():
            for subject, count in data['subjects'].items():
                rows.append({'AI4CE': 'AI4CE', 'Level': level,
                            'Subject': subject, 'Count': count})
        return pd.DataFrame(rows)

    def create_treemap(df):
        # Define a color map for levels, including the root
        color_discrete_map = {
            'AI4CE': '#ff00ff', 'Bachelor': '#ff00ff', 'Master': '#FDC443', 'Other': '#055471'}

        # Create the treemap
        fig = px.treemap(df, path=['AI4CE', 'Level', 'Subject'], values='Count',
                         color='Level', title='Distribution of Students by Level and Subject under AI4CE',
                         color_discrete_map=color_discrete_map)

        # Update layout to change background color
        fig.update_layout(
            paper_bgcolor='white',  # You can change 'white' to any other color code you prefer
            # Ensures the plot area background matches the overall figure background
            plot_bgcolor='white'
        )
        return fig

    # stacked bar

    def create_stacked_bar_chart(df: pd.DataFrame):
        df_exploded = df.explode('stud_subject')
        # Aggregate data by level and subject
        summary_df = df_exploded.groupby(
            ['level', 'stud_subject']).size().reset_index(name='counts')
        fig = px.bar(summary_df, x='level', y='counts', color='stud_subject',
                     title='Stacked Bar Chart of Study Levels and Subjects',
                     labels={'counts': 'Number of Subjects'}, barmode='stack')
        return fig

    # ====================================================================
    # =================== Web App starts here ============================
    # ====================================================================
    # Configure the layout of the Streamlit page to 'wide'
    st.title("AI4CE Student Research Projects")

    # Load the project data
    df = load_data(MODULE_DESCRIPTION_PATH)

    col1, col2 = st.columns(2)
    with col1:
        start_date = st.date_input("Select Start Date", min_value=datetime(2023, 1, 1),
                                   value=datetime(2023, 1, 1), key="start_date", format="YYYY-MM-DD")
        start_date = pd.Timestamp(start_date)

    with col2:
        end_date = st.date_input("Select End Date", min_value=datetime(2023, 1, 2),
                                 max_value=datetime(2026, 12, 31),
                                 value=datetime.now().date(), key="end_date", format="YYYY-MM-DD")
        end_date = pd.Timestamp(end_date)

    # ================================================
    # ================ Metrics  ======================
    # ================================================
    # Calculate the number of active modules and total students
    total_modules, total_students, active_modules, active_students, today_modules, today_students = count_modules_and_students(
        start_date, end_date, df)

    with col1:
        col101, col102 = st.columns([2, 2])
        col101.header("Total")
        col11, col12, col13, col14 = st.columns([1, 1, 1, 1])
        with col11:
            st.metric("Modules", total_modules)
        with col12:
            st.metric("Students", total_students)
        col102.header("In Time Window")
        with col13:
            st.metric("Modules", active_modules)
        with col14:
            st.metric("Students", active_students)

    with col2:
        col2.header("Today")
        col21, col22 = st.columns([1, 1])
        with col21:
            st.metric("Number of Modules in Time Window", today_modules)
        with col22:
            st.metric("Number of Students in Time Window", today_students)

    style_metric_cards()

    modules_students_timeseries = count_modules_and_students_timeseries(
        start_date, end_date, df)
    # st.dataframe(modules_students_timeseries)

    col_bars, col_metrics = st.columns([2, 1])
    with col_bars:
        fig = plot_module_activity(df, start_date, end_date)
        st.plotly_chart(fig)
        with st.spinner('Generating plot...'):
            data_per_day = count_active_modules_per_day(
                df, start_date, end_date)
            fig = px.bar(data_per_day, x='Date', y='Active Modules', title='Distribution of Active Modules Over Time',
                         labels={'Active Modules': 'Sum of Active Modules', 'Date': 'Date'})
            fig.update_layout(
                autosize=False,
                width=950,
            )
            st.plotly_chart(fig)
            st.success('Plot generated!')
    with col_metrics:
        st.subheader("Total")
        st.metric("Modules", total_modules)
        st.metric("Students", total_students)
        st.header("In Time Window")
        st.metric("Modules", active_modules)
        st.metric("Students", active_students)
        st.header("Today")
        st.metric("Number of Modules in Time Window", today_modules)
        st.metric("Number of Students in Time Window", today_students)

    # Plotting module activity within the selected dates
    # with col1:
    #     st.plotly_chart(fig)

    # with col2:
    #     with st.spinner('Generating plot...'):
    #         data_per_day = count_active_modules_per_day(df, start_date, end_date)
    #         fig = px.bar(data_per_day, x='Date', y='Active Modules', title='Distribution of Active Modules Over Time',
    #                     labels={'Active Modules': 'Sum of Active Modules', 'Date': 'Date'})
    #         st.plotly_chart(fig)
    #         st.success('Plot generated!')

    col31, col32 = st.columns([1, 1])

    type_counts = df['type'].value_counts()
    subjects = df['stud_subject'].explode()

    with col31:
        fig = px.pie(subjects, names='stud_subject',
                     title='Distribution of Study Subjects')
        fig.update_traces(textposition='inside',
                          textinfo='percent+label+value', hoverinfo='label+percent+value')
        # make piechart bigger
        fig.update_layout(
            autosize=False,
            width=800,
            height=800,
        )
        st.plotly_chart(fig)

    with col32:
        # Custom color palette for the space or AI theme
        colors = ['#1f77b4',  # muted blue
                  '#ff7f0e',  # safety orange
                  '#2ca02c',  # cooked asparagus green
                  '#d62728',  # brick red
                  '#9467bd',  # muted purple
                  '#8c564b',  # chestnut brown
                  '#e377c2',  # raspberry yogurt pink
                  '#7f7f7f',  # middle gray
                  '#bcbd22',  # curry yellow-green
                  '#17becf']  # blue-teal

        # Plotting the distribution using a pie chart with custom colors
        fig = px.pie(values=type_counts.values, names=type_counts.index, title='Distribution of Module Types',
                     color_discrete_sequence=colors[:len(type_counts)])  # Use as many colors as needed
        fig.update_traces(textposition='inside',
                          textinfo='percent+label+value', hoverinfo='label+percent+value')
        fig.update_layout(
            autosize=False,
            width=800,
            height=800,
        )

        # Display the plot in Streamlit
        st.plotly_chart(fig)

    st.dataframe(df, width=1600, use_container_width=True, height=666)

    # # TreeMap
    # df_exploded = df.explode('level')
    # fig = px.treemap(df_exploded, path=['level', 'type', 'stud_subject'], values='team_size',
    #                     color='level', title='Distribution of Study Levels and Subjects')
    # st.plotly_chart(fig)

    # SunBurst
    df_exploded = df.explode('stud_subject')
    fig = px.sunburst(df_exploded, path=['level', 'stud_subject'], values='team_size',
                      color='level', title='Sunburst Chart of Study Levels and Subjects')
    fig.update_layout(
        autosize=False,
        width=800,
        height=800,
    )
    st.plotly_chart(fig)

    # TreeMap
    fig = px.treemap(df_exploded, path=['level', 'stud_subject'], values='team_size',
                     color='level', title='Distribution of Study Levels and Subjects')
    st.plotly_chart(fig)

    level_and_subject_counts = load_and_count_levels_and_subjects(
        MODULE_DESCRIPTION_PATH)
    df_treemap = prepare_data_for_treemap(level_and_subject_counts)
    fig = create_treemap(df_treemap)
    st.plotly_chart(fig)

    st.plotly_chart(create_stacked_bar_chart(df))

    st.dataframe(df, width=1600, use_container_width=True, height=666)
    pass

    df = px.data.stocks(indexed=True)-1
    fig = px.area(df, facet_col="company", facet_col_wrap=2)
    fig.show()

    st.divider()
    st.divider()
    st.title("Coming up soon ..")
    st.header("- Total lines of code")
    st.header("- Endpoints at backend")


dashboard()
# if __name__ == "__main__":
#     # st.set_page_config(layout="wide")

#     dashboard()
