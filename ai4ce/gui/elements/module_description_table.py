import tomllib
import streamlit as st


def module_table():
    # Replace 'yourfile.toml' with the path to your TOML file
    filepath = 'ai4ce/module_description.toml'

    # Reading and loading the TOML file
    with open(filepath, 'rb') as file:
        data = tomllib.load(file)

    # Check if 'modules' key exists in data to avoid KeyError
    if 'modules' in data:
        # Start creating the table header
        module_table = "| Name | Description |\n|------|-------------|\n"

        # Iterate over each module in the modules list
        for module in data['modules']:
            # Extract name and description for each module, ensuring they exist
            # Default to 'No Name' if not found
            name = module.get('name', 'No Name')
            # Default to 'No Description' if not found
            description = module.get('description', 'No Description')

            # Append each module's details to the table string
            module_table += f"| {name} | {description} |\n"

        # Display the table in Streamlit
        st.markdown(module_table)
