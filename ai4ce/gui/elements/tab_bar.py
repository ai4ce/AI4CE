from dataclasses import dataclass

import streamlit as st


@dataclass
class App:
    name: str
    title: str
    description: str


def app():
    if 'project_name' not in st.session_state:
        st.session_state['project_name'] = "None Selected"
        st.session_state['project_id'] = None

    # Add some space at the top before the logos
    # st.write('\n\n')

    app_dict = {
        0: App(
            name="AI4CE",
            title="AI4CE",
            description="Welcome to the AI4CE System Generator!"
        ),
        99: App(
            name="Create Project",
            title="Create Project",
            description="Create a new CubeSat project."
        ),
        # 1: App(
        #     name="System Architecture",
        #     title="CubSat System Architecture: Definition of Evaluation Functions",
        #     description="The CubSat System Architecture project, where Johannes developed the underlying evaluation functions for each CubeSat compoents."
        # ),
        2: App(
            name="g3Del",
            title="g3Del - Generative 3D model: See a conceptual 3D model of your satellite",
            description="The g3Del team lead by Moritz developed a tool that can generate 3D models for a given component list."
        ),
        # 3: App(
        #     name="Backend",
        #     title="Backend",
        #     description="This is the Backend App."
        # ),
        # 4: App(
        #     name="Requirements",
        #     title="Requirements",
        #     description="This is the Requirements App."
        # ),
        5: App(
            name="Build System Generator",
            title="Reinforcement Learning",
            description="With the Reinforcement Learning module developed by Younes"
                        "the user can create the System Generators."
                        "\n\nThis means that the AI agents can be created with all"
                        "their hyperparametrs and before the training process can"
                        "be started.XXXXX",
        ),
        6: App(
            name="AI Benchmarking",
            title="AI Benchmarking",
            description="This is the AI Benchmarking App."
        ),
        # 7: App(
        #     name="CS Benchmarking",
        #     title="CS Benchmarking",
        #     description="This is the CS Benchmarking App."
        # ),
        # 8: App(
        #     name="System Diagram",
        #     title="System Diagram",
        #     description="This is the System Diagram App."
        # ),
        9: App(
            name="Flight Dynamics",
            title="Flight Dynamics",
            description="This is the Flight Dynamics App."
        ),
        10: App(
            name="BP compdb2",
            title="BP compdb2",
            description="This is the BP compdb2 App."
        ),
        11: App(
            name="MBSE Export",
            title="MBSE Export",
            description="This is the MBSE Export App."
        ),
        12: App(
            name="geseto",
            title="geseto",
            description="This is the GESETO App."
        ),
        13: App(
            name="Ops 2 Design",
            title="Ops 2 Design",
            description="This is the Ops 2 Design App."
        ),
        16: App(
            name="DTreqs",
            title="Digital Twin Requirements",
            description="Making the first steps towards a digital twin world"
        ),
        98: App(
            name="Project Summary",
            title="Project Summary",
            description="A summary of the whole project."
        ),
        66: App(
            name="AI4CE Playground",
            title="AI4CE Playground",
            description="A playground to test new concepts"
        ),
        77: App(
            name="Repo Template",
            title="Repo Template",
            description="The default template for all repos"
        ),
    }

    menu_data = [{'id': app[0], 'label': app[1].name}
                 for app in list(app_dict.items())]
    menu_data[0].update({'icon': "far fa-copy"})

    over_theme = {'txc_inactive': 'white',
                  'txc_active': 'black', 'menu_background': '#055471'}
    menu_id = hc.nav_bar(
        menu_definition=menu_data,
        override_theme=over_theme,
        hide_streamlit_markers=True,  # hide when deployed
        sticky_nav=True,  # at the top or not
        sticky_mode='pinned',  # jumpy or not-jumpy, but sticky or pinned
    )

    match menu_id:
        case 0 | 'AI4CE':  # for some reason the menu_id starts as 0, but subsequently becomes 'AI4CE'
            welcome()

            dashboard()

            plantuml_url = "https://cdn-0.plantuml.com/plantuml/png/VLHHRzis47xdhpWWmE06o9PsNH_aB6WYDKMn10lYRNanC33IMU8gaXf9nNFd_VVnHB5q5ALvOlHzTv_lxhjxRnqthfSj6nuCm6w5AnjOv0Qvm_srvXNS6lqNbkwLXm5ARX4MPrPymUprs_FZuy0yFZxxWRwT1nP0RNJVmMfd7Kgu5r-ugTW80Hnj6n-KXT2O7uI2BZoBosNYNekwSHyDeifMs1AHM-2b4uzVGnCxAt0Z54RX2rCsmdb6Rt0vaO_K6s6jq0f-DG_YCDDGk-VmldLmXrjV0LprgdIUV43xK8blmIkd3KxnsL9HLQ5hSntyY0eDzxb6C4vg0R7XSJYBEb1nhRX_kaCSO6nXocUNJ_5SguseUuClKtupN2YueEGHIkgjxzfQILrXEr6kqS7m7JP6oxKQ6m2n0sCFGbq7_Vq7ozuXt51eOXhiaBkd2biZFB7Kick5TU0q98rf96mRqI8ihhG1v6K3TcWCLc1t5iOUqgWakaPNhq1OICrEivhT2RBEP2Fczk55Wy-x-wZZBTO3ZmHUz7XoED7c_Psj6hq5EK8wGQcoRtemWNVNWEfH6AqaAWUwewxPaRBHvZERU6-jYZnV7X20rroeI3iadhx-C_VYLE7B0OcUzzPfUR1FWVslDl_DpYsj-MG_xUpv2TJ1_LW5FyxpheINkKJhG461LjJAmbe1BqjjAg5gScOTc_diBAtu8oOtLcEyKDx4Fia9tFIj4rsBAOM53atqEd56yHXCS6B3yuEJrevbuvGHVYn0XXfP66CWmyKdABXj7dIoC41seNYxiy9wcEHyXEIgHD9AEfGfZI-v2sBqa4ziO97t3xZYxXMu1jKJl-Mhn7wcJcWeUlwgOclnIROQBJezayZ_gaacW30QJXGdIXh99Iy_UuLvCVfFRDYaqM4ds5XxBoKtEqO-5DR5JRl3ltjXK8uNDe1XWK6eZHxqZkw3NMYHDK7Jhyb4ipuWUk1z1xcM7JS8l-1kT2mKHZnwEY-DjZP6XVFeKDeKdNmIjobso0wNdaO83wbHoM5qhnqgkiZQE8_HjQSdBSXjSRLa1zPfiQR_3AqP-xshwCTayERqpTkJqtSdfp-nFz3GCSlWzFjtZEr_pdMhpPxAAOqOhXBisVxiA1_-pi7lwWxsq7UbbhIK18OLFFUjvgf6Zzv_kWr0MkyX4jKUpafIoCB7GYlqJ78rvSs-vcK0EcJBA5kC2a9t7me68P-7AGUL74fbZ5cdE_RUltlP_Wy0"
            # iframe_code = f'<iframe src="{plantuml_url}" width="700" height="400"></iframe>'
            iframe_code = f'<iframe src="{plantuml_url}" width=1000 height=3000></iframe>'
            st.markdown(iframe_code, unsafe_allow_html=True)

            # Replace 'yourfile.toml' with the path to your TOML file
            filepath = 'ai4ce/module_description.toml'

            # Reading and loading the TOML file
            with open(filepath, 'rb') as file:
                data = tomllib.load(file)

            # Check if 'modules' key exists in data to avoid KeyError
            if 'modules' in data:
                # Start creating the table header
                module_table = "| Name | Description |\n|------|-------------|\n"

                # Iterate over each module in the modules list
                for module in data['modules']:
                    # Extract name and description for each module, ensuring they exist
                    # Default to 'No Name' if not found
                    name = module.get('name', 'No Name')
                    # Default to 'No Description' if not found
                    description = module.get('description', 'No Description')

                    # Append each module's details to the table string
                    module_table += f"| {name} | {description} |\n"

                # Display the table in Streamlit
                st.markdown(module_table)
            else:
                st.write("No modules found in the TOML file.")

                st.write("\n")

        case 98:
            project_summary()

        case 99:
            # st.title("Rebuild or Create a new CubeSat project")

            col11, col12 = st.columns(2)
            sys_config_enabled = {}

            with col11:
                st.title("Load a Use Case")
                st.markdown("""
                            To showcase the functionality of the AI4CE system generator hub, selected Use Cases can be loaded. This includes
                            - Project, Mission/Orbit and System Description
                            - pre-trained AI models
                            - pre-run brute force algorithm
                            """)
                col111, col112 = st.columns(2)
                with col111:
                    st.image(
                        "https://www.thalesgroup.com/sites/default/files/database/assets/images/2021-12/OPS-SAT_ESA_s_flying_lab_open_to_all_pillars_1100.jpg")
                with col112:
                    st.subheader("ESA's OPS-Sat CubeSat - 3comp")
                    st.write(
                        "OPS-Sat is a 3U CubeSat mission by ESA/ESOC to demonstrate new ways to handle on-board experiments.")

                    # st.markdown(
                    #     ''' **battery, solar-panel, reaction-wheel**''')

                    st.toggle("Battery", value=True, key="3comp_bat")
                    st.toggle("Camera", value=False, key="3comp_cam")
                    st.toggle("Reaction Wheel", value=True, key="3comp_rw")
                    st.toggle("Solar Panel", value=True, key="3comp_sp")
                    st.toggle("Transceiver", value=False, key="3comp_tran")

                    if st.button("Load OPS-Sat 3comp"):

                        project_config = load_file(filename=Path(
                            "ai4ce/project_config_opssat.toml"))
                        # st.write(project_config["Project"]["Project_Name"])

                        project_info = {}
                        project_info["project_name"] = project_config["Project"]["Project_Name"]
                        project_info["description"] = project_config["Project"]["Description"]
                        project_info["website"] = project_config["Project"]["Website"]
                        project_info["picture_web"] = project_config["Project"]["Picture_web"]
                        project_info["project_opensource"] = False

                        enabled_components = get_enabled_components(
                            project_config)
                        st.expander("Enabled Components").write(
                            enabled_components)

                        msg = set_new_project(project_info=project_info)
                        # st.write(msg)
                        st.session_state['project_id'] = msg["id"]
                        st.session_state['project_name'] = msg["project_name"]
                        # st.write(f"{st.session_state['project_id']} - {st.session_state['project_name']}")

                        # Configure a dict, that can be parsed by the backend according to a project config toml

                        # Find all enabled components based on the project toml
                        # bring it in the form as it is expected by the v2 backend
                        # set sys arch in backend for the project
                        # sys_config_enabled = {}
                        # # Loading System Configuration
                        # for key, value in project_config["System"].items():
                        #     if isinstance(value, dict):
                        #         traverse_and_modify(d=value, sys_config_enabled=sys_config_enabled)

                        # st.markdown(''' **Set Sys Arch**''')
                        # # resp = set_sys_arch(project_id=st.session_state["project_id"], sys_arch=sys_config_enabled)
                        # # st.expander("Response", expanded=False).write(resp)
                        # # st.write(resp)

                        resp = translate_tomlsystem_to_backend(
                            system=project_config["System"], project_id=st.session_state["project_id"])
                        st.expander("Response", expanded=False).write(resp)

                        with st.expander("Get Sys Arch from backend"):
                            st.write(get_sys_arch(
                                project_id=st.session_state["project_id"]))

                        st.success(
                            f"Set {st.session_state['project_name']} as active project @ project ID {st.session_state['project_id']}")
                        # st.success("OPS-Sat project loaded successfully.")

                col121, col122 = st.columns(2)
                with col121:
                    st.image(
                        "https://www.thalesgroup.com/sites/default/files/database/assets/images/2021-12/OPS-SAT_ESA_s_flying_lab_open_to_all_pillars_1100.jpg")
                with col122:
                    st.subheader("ESA's OPS-Sat CubeSat - 5comp")
                    st.write(
                        "OPS-Sat is a 3U CubeSat mission by ESA/ESOC to demonstrate new ways to handle on-board experiments.")
                    # st.markdown(
                    #     ''' **battery, solar-panel, reaction-wheel, transceiver, camera**''')

                    st.toggle("Battery", value=True)
                    st.toggle("Camera", value=True)
                    st.toggle("Reaction Wheel", value=True)
                    st.toggle("Solar Panel", value=True)
                    st.toggle("Transceiver", value=True)

                    if st.button("Load OPS-Sat 5comp"):
                        project_config = load_file(filename=Path(
                            "ai4ce/project_config_opssat.toml"))
                        # st.write(project_config["Project"]["Project_Name"])

                        project_info = {}
                        project_info["project_name"] = project_config["Project"]["Project_Name"]
                        project_info["description"] = project_config["Project"]["Description"]
                        project_info["website"] = project_config["Project"]["Website"]
                        project_info["picture_web"] = project_config["Project"]["Picture_web"]
                        project_info["project_opensource"] = False

                        enabled_components = get_enabled_components(
                            project_config)
                        st.expander("Enabled Components").write(
                            enabled_components)

                        msg = set_new_project(project_info=project_info)
                        # st.write(msg)
                        st.session_state['project_id'] = msg["id"]
                        st.session_state['project_name'] = msg["project_name"]

                        resp = translate_tomlsystem_to_backend(
                            system=project_config["System"], project_id=st.session_state["project_id"])
                        st.expander("Response", expanded=False).write(resp)

                        with st.expander("Get Sys Arch from backend"):
                            st.write(get_sys_arch(
                                project_id=st.session_state["project_id"]))

                        st.success(
                            f"Set {st.session_state['project_name']} as active project @ project ID {st.session_state['project_id']}")

            project_info = {}

            with col12:
                st.title("Create a new CubeSat project")
                project_info["project_name"] = st.text_input("Project Name")
                project_info["project_description"] = st.text_area(
                    "Project Description")
                project_info["project_website"] = st.text_input(
                    "Project Website", placeholder="https://ai4ce.space")
                project_info["project_picture"] = st.text_input(
                    "Project Picture", placeholder="https://ai4ce.space/cube_sat_rendering.png")
                project_info["project_opensource"] = st.radio(
                    label="Is your project open source?", options=["Yes.", "Not yet."], horizontal=True)

                if st.button("Create a new Project.", key="custom_button", args={"class": "custom-button"}):
                    msg = set_new_project(project_info=project_info)
                    st.success(msg)
                st.divider()

                clicked = st.button("Create 7 Example projects.", key="button", args={
                                    "class": "custom-button"})

                if clicked:
                    # URL and query parameters
                    url = "http://backend:8000/api/v1/testing/create_project"
                    params = {
                        "number_of_projects": 7,
                        "max_components": 10,
                        "create_components": "true"
                    }

                    # Headers
                    headers = {
                        "accept": "application/json"
                    }

                    # Make the POST request
                    response = httpx.post(
                        url, params=params, headers=headers, content='')

                    st.write(response.json())

            st.divider()
            # plantuml_url = "https://www.plantuml.com/plantuml/png/bLInRjim4Dtv5G_8OWyfqY3e82znx2O8aA11tJQB5qOwIsmfKY1FSLFquqjAfbKRCefkfDwxTxntpxxsB1stjHRTGG1-gpYlC5auaao_7wmiyEJiTyfv6c0WbvuIt7y1IcVR1gitprJZMVsIhX0xw6nR1MRMyvCIb47J7vStiyHyK6N5Tux8P2lIaIOzPCxgTL-OkDcIDifGQZbtUQKuuAsZsK7ZH7nKtYjhyCczg46bcoumPAiPdsahNO7RrkGSg7lInqAT1bTi7HsZe-EHANgVnZox8qDE1gqTcFOmGR8uMJyfUogjZGn7_Xi7ad5xFJyxn1VMR5GPR3fD-UAaCfX7yGGDtNLmR6rgMv0-wbmIe_kEZRFrskmCG7GWCQC9_M02IFR-fBpbiA5OEnEE6f9yABrrAX1oMpTQUGPR3EAf0jjAQSBarZgGp2lupWaguDyyUjEwtTJ4bIscK1xHtP7jf8p7T4p5MBPF1tOqhMUDr5HslAZvxzHsjEFC_b-0qkk6hjrUgKX6Z_5kLUXnGvvXi2IlIkEnDf1vRbsXJ1cjBrEdlKHwz4g-qc1xiQjN9cmfY9pZiTMi6as3X4T3BYqpSbBpL1pWWOsndzAGg8rrFqIoPNwFfVJLYntM1MHp8_MRLpx2GJfKrDBadVc3PYS4MGQt10QNaOexERN1PD6-q4ho55oHELZAOhLdZpGFQ3_V-5y3QKy7QYLvOZiY55vrKYdTrEPWk7ZgZ192KnblBgvVV6qAoLHakBoul3g_k3f__q5y8nVpb-7YtQK0h9Ec96HuldyArounuRQqXg9iBnTqFTj6N8TpM-i_"
            # con1 = st.container(height=1700)
            # iframe_code = f'<iframe src="{plantuml_url}" width="1200" height="1700"></iframe>'
            # con1.markdown(iframe_code, unsafe_allow_html=True)

        case x if x in list(app_dict.keys())[1:]:
            st.title(app_dict[x].title)
            st.write(app_dict[x].description +
                     f"\n\nYou can see the project in Fullscreen here: {base_url}:{str(8500 + x)}")
            st.markdown(
                f'<iframe src="{base_url}:{str(8500 + x)}" width="100%" height="1700px"></iframe>', unsafe_allow_html=True)
        case _:
            st.markdown('Something went wrong...')
