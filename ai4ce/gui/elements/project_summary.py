
import json
from pathlib import Path
import streamlit as st
import plotly.graph_objects as go


from ai4ce.helpers import get_list_of_all_project_infos, get_prepared_system_generator_info, get_recent_project, get_sys_arch, load_file, title_with_project_selector


def project_summary():
    if 'project_name' not in st.session_state:
        st.session_state['project_name'] = "None Selected"
        st.session_state['project_id'] = None
        st.session_state["modified"] = None
    title_with_project_selector(
        f"Summary of the System Generation for {st.session_state['project_name']}")

    # proj_list = get_list_of_all_project_infos()
    # project_idname_list = [(project["id"], project["project_name"], project["modified"])
    #                        for project in proj_list]

    # # st.session_state["project_name"] = None
    # col01, col02, col03, col04 = st.columns([5, 1, 2, 2])
    # with col01:
    #     st.title(
    #         f"Summary of the System Generation for {st.session_state['project_name']}")

    # with col02:
    #     if st.button("Get recent project"):
    #         (st.session_state["project_id"],
    #          st.session_state["project_name"],
    #          st.session_state["modified"]) = get_recent_project()
    # with col03:
    #     # Let the user select a project from the list of projects in the project DB
    #     selected_project = st.selectbox("Select a project",
    #                                     project_idname_list,
    #                                     index=None,
    #                                     placeholder=st.session_state["project_name"])
    #     if selected_project is not None:
    #         st.session_state["project_id"] = selected_project[0]
    #         st.session_state["project_name"] = selected_project[1]
    # with col04:
    #     st.metric(label="Active project",
    #               value=st.session_state['project_name'])

# =============================================================================
    col11, col12, col13 = st.columns([1, 1, 1])

    with col11:
        con1 = st.container(border=True, height=950)
        with con1:
            st.subheader("System Generators")
            system_generators: list = get_prepared_system_generator_info(
                st.session_state["project_id"])
            con11 = st.container(border=True)
            con11.metric(label="Amount of Stored System Generators",
                         value=len(system_generators))

            sys_gen_sel = st.selectbox(
                "Select a System Generator", options=system_generators)
            st.expander("See generator details",
                        expanded=False).write(sys_gen_sel)

            component_id: int = 4944371
            df = load_file(Path("ai4ce/gui/component_db_satsearch.csv"))

            for component in sys_gen_sel["generated_designs"][2]["comp_list_uids_temp"]:

                # Filter the DataFrame for the specified component ID
                col1, col2 = st.columns([1, 1])
                component_data = df[df['id'] == component[0]]
                with col1:
                    st.header(eval(component_data["tags"].iloc[0])[-1])
                    st.write(component_data["name"].iloc[0])
                    st.write(component_data["id"].iloc[0])
                    st.write(component_data["href"].iloc[0])
                # st.write(eval(component_data["specs"].iloc[0]))
                st.dataframe(
                    eval(component_data["specs"].iloc[0]), use_container_width=True)

                with col2:
                    if eval(component_data["tags"].iloc[0])[-1] == "solar-panels":
                        st.image(
                            "https://cdn.satsearch.com/product-images/satsearch_product-image_u1pngt_npcspacemind_sm-sp3x.png")
                    if eval(component_data["tags"].iloc[0])[-1] == "reaction-wheel":
                        st.image(
                            "https://cdn.satsearch.com/product-images/satsearch_product-image_8uwj7j_cubespace_cubewheel-cw0017.jpg")
                    if eval(component_data["tags"].iloc[0])[-1] == "battery":
                        st.image(
                            "https://cdn.satsearch.com/product-images/satsearch_product-image_fwt7h3_exa_1u-high-capacity-battery-arrays.JPG")

                # Check if the DataFrame is empty (i.e., the component ID was not found)
                if component_data.empty:
                    print(f"No data found for component ID: {component_id}")
                    return None

                # st.write("Component Summary:")
                # st.dataframe(
                #     component_data, hide_index=True, use_container_width=True)
                st.divider()

            with st.expander("Show all generators and their details", expanded=False):
                for sys_gen in system_generators:
                    st.write(sys_gen)
    with col12:
        con2 = st.container(border=True, height=950)
        with con2:
            st.subheader("System Architecture")
            sys_arch: dict = get_sys_arch(
                project_id=st.session_state["project_id"])
            st.expander("System Architecture").write(sys_arch)

            sub_systems: list = ["aodcs", "com", "eps",
                                 "obs", "payload", "structure", "tcs"]
            non_sub_systems: list = ["eFunction", "requirements"]
            for sub_system in sub_systems:
                st.subheader(sub_system)
                st.toggle(label="Enabled?",
                          value=sys_arch[sub_system]["enabled"], key=f"toggle_{sub_system}_sys")

                col_elem, col_toggle = st.columns([1, 1])
                for key, value in sys_arch[sub_system].items():
                    if isinstance(value, dict) and key not in non_sub_systems:
                        col_elem.write(key)
                        col_toggle.toggle(label="Enabled?",
                                          value=sys_arch[sub_system][key]["enabled"], key=f"toggle_{key}")
                        pass

                pass
                st.divider()

    with col13:
        con3 = st.container(border=True)
        with con3:
            st.header("Orbit")
            # with open(str(Path(__file__).parent.absolute())+'/graphics'+'/orbit_plot.json', 'r') as f:
            with open(str(Path(__file__).parent.parent.absolute())+'/orbit_plot.json', 'r') as f:
                graph_json = json.load(f)

            fig = go.Figure(graph_json)
            st.plotly_chart(fig, use_container_width=True)


if __name__ == "__main__":
    st.set_page_config(layout="wide", page_title="AI4CE System Generator")
    project_summary()
