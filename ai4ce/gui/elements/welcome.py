import streamlit as st


def welcome():
    # Create three columns
    col1, col2, col3 = st.columns([3, 1, 1])

    with col1:
        # st.title("AI4CE")
        st.markdown('<h1 style="font-size: 5em;">AI4CE</h1>',
                    unsafe_allow_html=True)
        st.write("Welcome to the AI4CE System Generator!")
        st.write("AI4CE is a research project at TU Darmstadt in cooperation with Parametry.ai. It interspects the feasibilty of using AI agents to generate system designs.")
        st.write("AI4CE provides answers to the following Research Questions (RQ):\n  - RQ1 How promissing is a System Generation based on Deep Reinforcement Learning?\n   - RQ2 What are the limitating factors of DRL for system generation?")
    # Place the first logo in the second column
    with col2:
        st.image('ai4ce/gui/graphics/logo_ai4ce.png', width=222)

    # Place the second and third logos in the third column
    with col3:
        st.image('ai4ce/gui/graphics/logo_celab.png', width=250)
        st.image(
            'ai4ce/gui/graphics/logo_parametry-ai-purple.png', width=250)

    col21, col22 = st.columns([3, 3])

    with col21:
        st.image('ai4ce/gui/graphics/ai4ce_platform_overview_20231031.png',
                 caption='Your caption here')

    with col22:
        # clicked = st.button("Create 7 Example projects.", key="custom_button", args={
        #                     "class": "custom-button"})

        # if clicked:
        #     # URL and query parameters
        #     url = "http://backend:8000/api/v1/testing/create_project"
        #     params = {
        #         "number_of_projects": 7,
        #         "max_components": 10,
        #         "create_components": "true"
        #     }

        #     # Headers
        #     headers = {
        #         "accept": "application/json"
        #     }

        #     # Make the POST request
        #     response = httpx.post(
        #         url, params=params, headers=headers, content='')

        #     st.write(response.json())

        with st.container(border=True):
            st.image(
                "ai4ce/gui/graphics/ai4ce_summary_demo_small.gif", output_format="GIF", use_column_width=True)
