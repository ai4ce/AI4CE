""" See https://www.youtube.com/watch?v=ZI7_INDzqbw """

import streamlit as st


def sidebar():

    p000 = st.Page(
        "gui.py",
        title="Home",
        icon=":material/query_stats:"
    )
    p001 = st.Page(
        "elements/modules.py",
        title="Modesl Overview Dashboard",
        icon=":material/area_chart:"
    )
    p01 = st.Page(
        "modules/01_CSarch.py",
        title="01_CSarch",
        icon=":material/query_stats:"
    )

    p02 = st.Page(
        "modules/02_g3Del.py",
        title="02_g3Del",
        icon=":material/query_stats:"
    )

    p03 = st.Page(
        "modules/03_Backend.py",
        title="03_Backend",
        icon=":material/query_stats:"
    )

    p04 = st.Page(
        "modules/04_Reqs.py",
        title="04_Reqs",
        icon=":material/query_stats:"
    )

    p05 = st.Page(
        "modules/05_RL.py",
        title="05_RL",
        icon=":material/query_stats:"
    )

    p06 = st.Page(
        "modules/06_AIbench.py",
        title="06_AIbench",
        icon=":material/query_stats:"
    )

    p07 = st.Page(
        "modules/07_CSbench.py",
        title="07_CSbench",
        icon=":material/query_stats:"
    )

    p08 = st.Page(
        "modules/08_SysDiagram.py",
        title="08_SysDiagram",
        icon=":material/query_stats:"
    )

    p09 = st.Page(
        "modules/09_FlyDyn.py",
        title="09_FlyDyn",
        icon=":material/query_stats:"
    )

    p10 = st.Page(
        "modules/10_CompDB2.py",
        title="10_CompDB2",
        icon=":material/query_stats:"
    )

    p11 = st.Page(
        "modules/11_MBSEexport.py",
        title="11_MBSEexport",
        icon=":material/query_stats:"
    )

    p12 = st.Page(
        "modules/12_geseto.py",
        title="12_geseto",
        icon=":material/query_stats:"
    )

    p13 = st.Page(
        "modules/13_OPS2Design.py",
        title="13_OPS2Design",
        icon=":material/query_stats:"
    )

    p14 = st.Page(
        "modules/14_DroneSysArch.py",
        title="14_DroneSysArch",
        icon=":material/query_stats:"
    )

    p16 = st.Page(
        "modules/16_DigitalTwin.py",
        title="16_DigitalTwin",
        icon=":material/query_stats:"
    )

    p18 = st.Page(
        "modules/18_LLMreport.py",
        title="18_LLMreport",
        icon=":material/query_stats:"
    )

    p19 = st.Page(
        "modules/19_Req2Sim.py",
        title="19_Req2Sim",
        icon=":material/query_stats:"
    )

    p20 = st.Page(
        "modules/20_eFuncs.py",
        title="20_eFuncs",
        icon=":material/query_stats:"
    )

    p21 = st.Page(
        "modules/21_PAML.py",
        title="21_PAML",
        icon=":material/query_stats:"
    )

    p22 = st.Page(
        "modules/22_orbitviz.py",
        title="22_orbitviz",
        icon=":material/query_stats:"
    )

    pg = st.navigation([p000, p001, p01, p02, p03, p04, p05, p06, p07, p08,
                       p09, p10, p11, p12, p13, p14, p16, p18, p19, p20, p21, p22])
    pg.run()

    st.sidebar.title("Backend")
    st.sidebar.markdown("http://localhost:8503")

    # pp01 = st.Page(
    #     "modules/01_CSarch.py",
    #     title="01_CSarch",
    #     icon=":material/query_stats:"
    # )

    # ppg = st.navigation([pp01])
    # ppg.run()


if __name__ == "__main__":
    sidebar()
