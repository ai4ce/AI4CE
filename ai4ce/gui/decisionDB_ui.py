import pandas as pd
import streamlit as st
from copy import deepcopy

st.set_page_config(layout="wide")

st.title("Decision Database Import Interface")

parameter_name_examples = {"mass": "kg",
                           "P_consump_max": "W",
                           "T_ops_max": "K",
                           "T_ops_min": "K",
                           "P_consump_min": "W",
                           "T_ops_avg": "K",
                           "P_consump_avg": "W",
                           "depth": "m",
                           "height": "m",
                           "width": "m",
                           }


system_type_examples = ["Satellite",
                        "CubeSat",
                        "Rover",
                        "Drone",
                        "Station",
                        "Neural Net",
                        "Tiny House",
                        ]

comp_type_examples = [
    "EPS",
    "Battery",
    "Solar Panel",
    "Antenna",
    "Actuator",
    "Transceiver",
    "Processor",
    "Sensor",
    "Power Management",
    "Thermal Management",
    "Structure",
    "Software"
]


if "comp_info" not in st.session_state:
    st.session_state["comp_info"] = {}
if "value_name_value" not in st.session_state:
    st.session_state["value_name_value"] = {}
if "value_name_value_official" not in st.session_state:
    st.session_state["value_name_value_official"] = {}
if "value_name_value_assumed" not in st.session_state:
    st.session_state["value_name_value_assumed"] = {}

col_info, col_picture = st.columns([2, 1])

with col_info:
    col1, col2 = st.columns([1, 1])
    with col1:
        comp_name: str = st.text_input(
            "Name of new Compoent", placeholder="Super Battery 50Wh")
        st.session_state["comp_info"]["comp_name"] = comp_name
        st.session_state["comp_info"]["comp_url"] = st.text_input(
            "URL", placeholder="https://product-url.space/")
        st.session_state["comp_info"]["picture_url"] = st.text_input(
            "Picture URL", placeholder="https://picture-url.space/")

    with col2:
        st.session_state["comp_info"]["sys_type"] = st.selectbox("Select System Type",
                                                                 options=system_type_examples,
                                                                 placeholder="Choose a System Type",
                                                                 index=None)
        comp_type = st.multiselect(
            "Select Comp Type", options=comp_type_examples)
        # st.session_state["comp_info"]["comp_type"] = comp_type
        st.session_state["comp_info"]["comp_type"] = ', '.join(comp_type)
        # st.write(type(st.session_state["comp_info"]["comp_type"]))

    st.dataframe(st.session_state["comp_info"], use_container_width=True)
# st.write(st.session_state["comp_info"])

# df = pd.DataFrame(st.session_state["comp_info"], index=[0]).T

# # Rename the columns
# df.reset_index(inplace=True)
# df.columns = ['Parameter', 'Value']
# st.dataframe(df, use_container_width=True, hide_index=True)

with col_picture:
    if st.session_state["comp_info"]["picture_url"] != "":
        st.image(st.session_state["comp_info"]["picture_url"],
                 caption="Picture of the Component", use_column_width=True)


st.header("Component Parameters")
col11, col12, col13, col14 = st.columns([2, 1, 1, 1])
with col11:
    parameter_name = st.selectbox(
        "Select parameter", options=parameter_name_examples.keys(), key="parameter", placeholder="Chosse a Parameter", index=None)
with col12:
    parameter_value: int | float = st.number_input(
        "Value", key="value")
    # parameter_value_num = st.number_input(label="Value")
with col13:
    st.write("Unit:")
    if parameter_name is not None:
        st.write(parameter_name_examples[parameter_name])
with col14:
    options = ['Official', 'Assumed', 'Calculated']
    parameter_origin = st.radio("What's the origin?", options)

col21, col22 = st.columns([1, 1])

with col21:
    # add a button to remove the values in the dictionary
    if st.button("Remove Value"):
        st.session_state["value_name_value"].pop(
            parameter_name, None)
with col22:
    if st.button("Add Value"):
        st.session_state["value_name_value"][parameter_name] = parameter_value

st.dataframe(
    st.session_state["value_name_value"], use_container_width=True, )


st.header("Calculated Parameters")
parameters_calculated = {}


def calculate_volume(parameters: dict) -> float:

    volume: float = parameters["depth"] * \
        parameters["height"]*parameters["width"]
    return volume


if st.button("Calculate Volume"):
    volume = calculate_volume(st.session_state["value_name_value"])
    st.session_state["value_name_value"]["volume"] = volume
    st.write(st.session_state["value_name_value"])


st.divider()
st.divider()
offi_para, assu_para = st.columns([1, 1])

with offi_para:
    st.header("Official Parameters")
    col11, col12, col13 = st.columns([2, 1, 1])
    with col11:
        parameter_name = st.selectbox(
            "Select parameter", options=parameter_name_examples.keys(), key="official_parameter", placeholder="Chosse a Parameter", index=None)
    with col12:
        parameter_value: int | float = st.text_input(
            "Value", key="official_value")
    with col13:
        st.write("Unit:")
        if parameter_name is not None:
            st.write(parameter_name_examples[parameter_name])

    col21, col22 = st.columns([1, 1])

    with col21:
        # add a button to remove the values in the dictionary
        if st.button("Remove Value"):
            st.session_state["value_name_value_official"].pop(
                parameter_name, None)
    with col22:
        if st.button("Add Official Value"):
            st.session_state["value_name_value_official"][parameter_name] = parameter_value

    st.dataframe(
        st.session_state["value_name_value_official"], use_container_width=True, )

# st.divider()
with assu_para:
    st.header("Assumed Parameters")
    col11, col12, col13 = st.columns([2, 1, 1])
    with col11:
        parameter_name = st.selectbox(
            "Select parameter", options=parameter_name_examples.keys(), key="assumed_parameter", placeholder="Choose a Parameter", index=None)
    with col12:
        parameter_value: int | float = st.text_input(
            "Value", key="assumed_value")
    with col13:
        st.write("Unit:")
        if parameter_name is not None:
            st.write(parameter_name_examples[parameter_name])

    col21, col22 = st.columns([1, 1])
    with col21:
        # add a button to remove the values in the dictionary
        if st.button("Remove Value", key="remove_assumed_value"):
            st.session_state["value_name_value_assumed"].pop(
                parameter_name, None)
    with col22:
        if st.button("Add Assumed Value", key="add_assumed_value"):
            st.session_state["value_name_value_assumed"][parameter_name] = parameter_value

    st.dataframe(
        st.session_state["value_name_value_assumed"], use_container_width=True)


st.divider()
# List of options
options = ['Official', 'Assumed', 'Calculated']
# Use st.radio to select 1 out of the 3 options
selected_option = st.radio("Choose one option:", options)
st.write(f"You selected: {selected_option}")

# Display the selected option
st.write(f"You selected: {selected_option}")

st.divider()


st.divider()

st.warning("TODO: Check for dublication of parameter names")
if st.button("Save in Backend"):
    st.warning("Here is your component uid: 1234567890")


pass
