from dataclasses import dataclass
import streamlit as st
import hydralit_components as hc

from ai4ce.gui.elements.sidebar import sidebar
from ai4ce.gui.gui import app


@dataclass
class App:
    name: str
    title: str
    description: str


# This check allows the script to be run standalone (useful for development/testing)
if __name__ == "__main__":
    st.set_page_config(layout="wide", page_title="AI4CE System Generator")
    sidebar()
    app()
