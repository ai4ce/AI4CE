import json
import typer
import subprocess
from pathlib import Path
from rich.prompt import Prompt
from typing import List, KeysView, Dict

import docker
import yaml

app = typer.Typer()

@app.command()
def up():
    """ A helper function to bring up the docker-compose.yml"""

    file_name = typer.prompt("Name of the docker compose", default="build_docker_compose.yml")
    subprocess.run(["docker", "compose", "-f", f"{file_name}","up", "-d"])

@app.command()
def down():
    """ A helper function to bring down the docker-compose.yml"""

    file_name = typer.prompt("Name of the docker compose", default="build_docker_compose.yml")
    subprocess.run(["docker", "compose", "-f", f"{file_name}","down"])


def parse_docker_compose_images(file_path: Path) -> Dict[str, str]:
    with file_path.open('r') as file:
        compose_dict = yaml.safe_load(file)
        services = compose_dict.get('services', {})
        return {service: details.get('image', f"{service}:latest") for service, details in services.items()}

def stop_docker_container(container_id: str, compose_file_name:str):
    try:
        # subprocess.run(["docker", "stop", container_id], check=True)
        subprocess.run(["docker", "compose", "-f", compose_file_name, "down", container_id])
        print(f"Container {container_id} stopped successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Failed to stop container {container_id}. Error: {e}")


def delete_docker_container(container_id: str):
    try:
        # Then remove the container
        subprocess.run(["docker", "rmi", container_id, "-f"], check=True)
        print(f"Container {container_id} removed successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Failed to delete container {container_id}. Error: {e}")


@app.command()
def rebuild():
    """ A helper function to bring down, rebuild and start up a service in the docker compose"""

    compose_file_name: str = typer.prompt("Name of the docker compose", default="build_docker_compose.yml")
    compose_file = Path(compose_file_name)

    # Check if docker-compose.yml exists
    if not compose_file.exists():
        typer.echo("Docker Compose file not found.")
        raise typer.Exit()

    # Parse images from Docker Compose file
    images: Dict[str, str] = parse_docker_compose_images(compose_file)
    if not images:
        typer.echo("No services found in Docker Compose.")
        raise typer.Exit()

    # Prepare choice list
    choices = ['[0] all'] + [f"[{i}] {image}" for i, image in enumerate(images.values(), start=1)]

    # Display choices each in a new line and ask user to select an image
    typer.echo("Which service would you like to bring down, rebuild and start back up:")
    for choice in choices:
        typer.echo(choice)
    selected = Prompt.ask("Enter your choice")

    # Identify selected service(s)
    if selected == '0':
        services_to_manage = images.keys()
    else:
        try:
            selected_index = int(selected) - 1
            services_to_manage = [list(images.keys())[selected_index]]
        except (ValueError, IndexError):
            typer.echo("Invalid choice")
            raise typer.Exit()

    typer.echo(f"Service(s) to manage: {services_to_manage}")
    typer.echo("========================================")

    for service in services_to_manage:
        typer.echo(f"Service = {service}")

        if "registry" in images[service]:
            typer.echo(f"Used image name: {images[service]}")
            image_to_restart = images[service]
            image_to_restart = images[service].split(":")[0]

            service_name = image_to_restart
            cmd = f"docker service ls --filter name={service_name} --format '{{{{.ID}}}}'"

            container_name = image_to_restart
            cmd = f"docker container inspect {'ai4ce-backend'} --format='{{{{.Image}}}}'"

            result = subprocess.run(cmd, shell=True, capture_output=True, text=True)
            image_id = result.stdout[7:]
            print(f"-----> {image_id}")

        else:
            image_to_restart = images[service]
            image_to_restart = images[service].split(":")[0]
        typer.echo(f"Used image name: {image_to_restart}")
        typer.echo(f"Service = {service}")
        typer.echo(f"Image = {images[service]}")

    # def get_docker_image_id(image_name: str) -> str:
        # try:
        #     if "registry" or "postgres" in image_to_restart:
        #         print(":-.-.-.-.-.-.:")
        #         print(image_to_restart)
        #         # image_info = images[image_to_restart]
        #         # print(image_info)
        #         # container_id = image_info[0]['Id'].split(':')[1][:5]
        #         # typer.echo(container_id)
        #         result = subprocess.run(
        #             ["docker", "inspect", "--type=image", image_to_restart],
        #             capture_output=True,
        #             text=True,
        #             check=True
        #         )
        #         print(type(result))
        #     else:
        #         result = subprocess.run(
        #             ["docker", "inspect", "--type=image", image_to_restart],
        #             capture_output=True,
        #             text=True,
        #             check=True
        #         )
        # except Exception as e:
        #     typer.echo(f"Error occurred here: {e}")
        # finally:
        #     typer.echo("Done with this step")


        typer.echo(f"____________________")
        typer.echo(images[service])


        # if "registry"|"postgres" in images[service]:
        # if "registry" in images[service]:
        #     stop_docker_container(container_id, compose_file_name)
        #     delete_docker_container(container_id)

        typer.echo(f"____________________")

        # if ":" in image_to_restart:
        #     # image_to_restart = image_to_restart.split(":")[0]
        #     image_to_restart = service
        if ":" in image_to_restart:
            image_to_restart = image_to_restart.split(":")[0]

        typer.echo(f"Stopping {image_to_restart} ...")
        # subprocess.run(["docker", "compose", "-f", compose_file_name, "down", service])
        subprocess.run(["docker", "compose", "-f", compose_file_name, "down", image_to_restart])
        # subprocess.run(["docker", "compose", "-f", compose_file_name, "down", "backend-backend"])

        typer.echo(f"Removing image {image_to_restart}...")
        # subprocess.run(["docker", "image", "rmi", images[service], "-f"])
        subprocess.run(["docker", "image", "rmi", f"ai4ce-{image_to_restart}", "-f"])
        subprocess.run(["docker", "image", "rmi", f"{'b97'}", "-f"])

        typer.echo(f"Rebuilding {image_to_restart}...")
        # subprocess.run(["docker", "compose", "up", "-d", service])
        subprocess.run(["docker", "compose", "-f", compose_file_name, "up", "-d", image_to_restart])
        # subprocess.run(["docker", "compose", "-f", compose_file_name, "up", "-d", "backend-backend"])

        # typer.echo(f"Starting {service}...")
        typer.echo(f"Starting {image_to_restart}...")
        # subprocess.run(["docker", "compose", "up", "-d", service])
        subprocess.run(["docker", "compose", "-f", compose_file_name, "up", "-d", image_to_restart])
        # input_string.split("-")[0]

    typer.echo("Service management complete.")


@app.command()
def update(
    repos_file: str = typer.Option(
        "repos.txt", help="Path to a file containing repository URLs, one per line."
    ),
    base_dir: str = typer.Option(
        None, help="Directory to clone repositories to."
    ),
):
    # Convert to Path objects and get absolute paths
    repos_file = Path(repos_file).resolve()
    base_dir = Path(base_dir).resolve() if base_dir else repos_file.parent.parent

    # Read repository URLs from the file
    with repos_file.open("r") as f:
        repos = f.read().splitlines()

    for repo_url in repos:
        repo_name = Path(repo_url).stem
        repo_path = base_dir / repo_name

        if repo_path.exists():
            typer.echo(f"Updating {repo_name}...")
            subprocess.run(["git", "-C", str(repo_path), "pull"])
        else:
            typer.echo(f"Cloning {repo_name}...")
            subprocess.run(["git", "clone", repo_url, str(repo_path)])

    typer.echo("All repositories are up to date.")


def echo_remote_branches(repo_path):
    """
    Retrieves and echoes a list of branches from the remote repository.
    """
    result = subprocess.run(["git", "-C", str(repo_path), "branch", "-r"], capture_output=True, text=True)
    if result.returncode != 0:
        typer.echo(f"Failed to fetch branches for {repo_path}")
        return []

    branches = result.stdout.splitlines()
    # Filter out branches, keeping only those that are from origin and not HEAD pointer
    valid_branches = [branch.strip().split('/')[1] for branch in branches if 'origin/' in branch and 'HEAD' not in branch]

    if valid_branches:
        # typer.echo(f"Available branches for {repo_path.name}: {', '.join(valid_branches)}")
        pass
    else:
        typer.echo(f"No branches found for {repo_path.name}")

    return valid_branches



def get_remote_branches(repo_path):
    """
    Retrieves a list of branches from the remote repository.
    """

    typer.echo(f"Possible branches for {repo_path.name}: {echo_remote_branches(repo_path)}")
    result = subprocess.run(["git", "-C", str(repo_path), "branch", "-r"], capture_output=True, text=True)
    branches = result.stdout.splitlines()
    # Filter out branches, keeping only those that are from origin and not HEAD pointer
    return [branch.strip().split('/')[1] for branch in branches if 'origin/' in branch and 'HEAD' not in branch]

def prompt_for_branch(branches, repo_name):
    """
    Prompts the user to choose a branch from the list of branches.
    """
    while True:
        branch = typer.prompt(f"Select a branch for {repo_name}", default="main", show_choices=True)
        if branch in branches:
            return branch
        typer.echo(f"Invalid branch. Please choose from: {', '.join(branches)}")

@app.command()
def update_branch(
    repos_file: str = typer.Option(
        # "repos.txt", help="Path to a file containing repository URLs, one per line."
        "repos.txt", help="Path to a file containing repository URLs, one per line."
    ),
    base_dir: str = typer.Option(
        None, help="Directory to clone repositories to."
    ),
):
    # Convert to Path objects and get absolute paths
    repos_file = Path(repos_file).resolve()
    base_dir = Path(base_dir).resolve() if base_dir else repos_file.parent.parent

    # Read repository URLs from the file
    with repos_file.open("r") as f:
        repos = f.read().splitlines()

    for repo_url in repos:
        repo_name = Path(repo_url).stem
        repo_path = base_dir / repo_name

        if repo_path.exists():
            typer.echo(f"Updating {repo_name}...")
            subprocess.run(["git", "-C", str(repo_path), "fetch"])
            branches = get_remote_branches(repo_path)
            if branches:
                branch = prompt_for_branch(branches, repo_name)
                subprocess.run(["git", "-C", str(repo_path), "checkout", branch])
            else:
                typer.echo(f"No branches found for {repo_name}")
        else:
            typer.echo(f"Cloning {repo_name}...")
            subprocess.run(["git", "clone", repo_url, str(repo_path)])
            branches = get_remote_branches(repo_path)
            if branches:
                branch = prompt_for_branch(branches, repo_name)
                subprocess.run(["git", "-C", str(repo_path), "checkout", branch])
            else:
                typer.echo(f"No branches found for {repo_name}")

        typer.echo("========================================")

    typer.echo("All repositories are up to date.")


@app.command
def dup():
    """ A helper function to bring up the docker-compose.yml"""

    file_name = typer.prompt("Name of the docker compose", default="build_docker_compose.yml")
    subprocess.run(["docker", "compose", "-f", f"{file_name}","up", "-d"])


if __name__ == "__main__":
    app()
