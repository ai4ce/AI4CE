# Demo: Recreating OPS-Sat for 3comp and 5comp system
What does each module need to enable a recreation of a reduced OPS-Sat project with AI4CE.


## 00_AI4CE

## 01_CSSysArch
- load system architecture
- load reward function

## 02_g3Del
- load comp_list
- load stl files

## 03_backend
- $ALL

## 04_requirements

## 05_RL
- load system architecture
- load reward functions
- load mission info

## 06_AIBench
- load brute force max reward
- load AI training log

## 07_CSBench
- load comp lists

## 08_SysDiagram

## 09_FlightDynamics
- load mission info

## 10_CompDB2

## 11_MBSEexport
- load comp list

## 12_geseto
- load comp list

## 13_OPS2Design
- load comp list
