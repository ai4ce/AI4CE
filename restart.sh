#!/bin/bash

# Step 1: Bring down the Docker Compose setup
echo "Bringing down the Docker Compose setup..."
docker compose -f build_docker_compose.yml down 
#

# Stopp all running containers
docker stop $(docker ps -a -q)
# Remove all containers
docker rm $(docker ps -a -q)
# Remove all docker images
docker rmi $(docker images -a -q)


# Step 2: Remove the Docker image
# echo "Removing the Docker image 'ai4ce'..."
# docker rmi ai4ce-ai4ce:latest

# docker build -t ai4ce:0.1 .

# Step 3: Bring up the Docker Compose setup in detached mode
echo "Bringing up the Docker Compose setup in detached mode..."
docker compose -f build_docker_compose.yml up -d


# docker logs -f ai4ce
